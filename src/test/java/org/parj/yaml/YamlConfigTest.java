package org.parj.yaml;

import org.junit.Before;
import org.junit.Test;
import org.parj.yaml.config.Config;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class YamlConfigTest {
    private Yaml yaml;
    private InputStream in;
    private Config config;

    @Before
    public void setupYaml() throws FileNotFoundException {
        yaml = new Yaml();
        in = new FileInputStream(new File("src/test/resources/yaml/config.yml"));
        config = yaml.loadAs(in, Config.class);
    }

    @Test
    public void loadYamlFileAsChange() {
        Config config = yaml.loadAs(in, Config.class);
    }

}
