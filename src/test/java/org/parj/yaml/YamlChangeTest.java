package org.parj.yaml;

import org.junit.Before;
import org.junit.Test;
import org.parj.yaml.change.Change;
import org.parj.yaml.change.Deployment;
import org.parj.yaml.change.Rollback;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class YamlChangeTest {

    private Yaml yaml;
    private InputStream in;
    private Change change;

    @Before
    public void setupYaml() throws FileNotFoundException {
        yaml = new Yaml();
        in = new FileInputStream(new File("src/test/resources/yaml/Rel_AN89812.yml"));
        change = yaml.loadAs(in, Change.class);
    }

    @Test
    public void loadYamlFileAsChange() {
        Change change = yaml.loadAs(in, Change.class);
    }

    @Test
    public void countApprovers() {
        assertEquals(change.getApprovers().size(), 3);
    }

    @Test
    public void countImpactServers() {
        assertEquals(change.getImpacted_servers().size(), 2);
    }

    @Test
    public void checkApproverIsArray() {
        assertEquals(change.getApprovers().getClass(), ArrayList.class);
    }

    @Test
    public void checkImpactedServersIsArray() {
        assertEquals(change.getImpacted_servers().getClass(), ArrayList.class);
    }

    @Test
    public void getAndCheckDeployment() {
        Deployment deployment = change.getDeployment();
        assertEquals(deployment.getVersion(), "1.5");
        assertEquals(deployment.getApp(), "app1");
        assertEquals(deployment.getCommand(), "deploy_app1");
    }

    @Test
    public void getAndCheckRollback() {
        Rollback rollback = change.getRollback();
        assertEquals(rollback.getVersion(), "1.1");
        assertEquals(rollback.getApp(), "app1");
        assertEquals(rollback.getCommand(), "deploy_app1");
    }

    @Test
    public void getChangeNumber() {
        assertEquals(change.getChange_number(), "AN89812");
    }
}
