package com.baeldung;

import org.parj.yaml.DeploymentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class RunProcess {
    public static final boolean isWindows = System.getProperty("os.name").indexOf("win") >= 0;
    private static final Logger logger = LoggerFactory.getLogger(RunProcess.class);

    public static DeploymentResponse run(String command) throws InterruptedException, IOException {
        logger.info("Executing deployment");

        ProcessBuilder builder = new ProcessBuilder();
        StringBuffer sysout = new StringBuffer();
        Consumer<String> consumer = (arg) -> { sysout.append(arg + System.lineSeparator()); };

        if (isWindows) {
            builder.command("cmd.exe", "/c", command);
        } else {
            builder.command("sh", "-c", command);
        }

        Process process = builder.start();
        StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), consumer);
        Executors.newSingleThreadExecutor().submit(streamGobbler);
        int exitCode = process.waitFor();

        return new DeploymentResponse(exitCode, sysout.toString());
    }
}
