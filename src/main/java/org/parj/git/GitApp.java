package org.parj.git;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.parj.Setup;
import org.parj.yaml.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Uses jgit (https://www.eclipse.org/jgit/) to connect to Git
 */
public class GitApp {
    private static Logger logger = LoggerFactory.getLogger(GitApp.class);
    /**
     * Clones repo using SSH key
     * @param repositoryUrl URL of repository
     * @param gitLocalPath local path to clone to
     * @return repository
     * @throws GitAPIException
     */
    public static Git cloneRepo(String repositoryUrl, String gitLocalPath) throws GitAPIException {
        logger.info("Cloning repo via SSH");
        TransportConfigCallback transportConfigCallback = new SshTransportConfigCallback();

        Git repository = Git.cloneRepository()
                .setURI(repositoryUrl)
                .setDirectory(new File(gitLocalPath))
                .setTransportConfigCallback(transportConfigCallback)
                .call();

        return repository;
    }


    /**
     * Clones repository using user and password
     * @param repositoryUrl URL of repository
     * @param gitLocalPath localpath to clone to
     * @param user user
     * @param password password
     * @return repository
     * @throws GitAPIException
     */
    public static Git cloneRepo(String repositoryUrl, String gitLocalPath, String user, String password) throws GitAPIException {
        logger.info("Cloning repo via HTTP(S)");
        Git repository = Git.cloneRepository()
                .setURI(repositoryUrl)
                .setDirectory(new File(gitLocalPath))
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(user, password))
                .call();

        return repository;
    }


    /**
     * Runs a git pull
     * @param localRepositoryPath Path to local repository
     * @return Git Pull Result
     * @throws IOException
     * @throws GitAPIException
     */
    public static PullResult pull(String localRepositoryPath) throws IOException, GitAPIException {
        logger.info("Pulling git repo via SSH - " + localRepositoryPath);
        File file = new File(localRepositoryPath);
        Repository localRepo = new FileRepository(file.getAbsolutePath() + "/.git");

        PullCommand pullCommand = new Git(localRepo).pull();
        return pullCommand.call();
    }

    /**
     * Runs a git pull with user and password
     * @param localRepositoryPath Path to local repository
     * @param user User of repo
     * @param password Password of repo
     * @return Git Pull Result
     * @throws IOException
     * @throws GitAPIException
     */
    public static PullResult pull(String localRepositoryPath, String user, String password) throws IOException, GitAPIException {
        logger.info("Pulling git repo via HTTP(S) - " + localRepositoryPath);
        File file = new File(localRepositoryPath);
        Repository localRepo = new FileRepository(file.getAbsolutePath() + "/.git");

        PullResult pullResult = new Git(localRepo).pull()
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(user, password))
                .call();

        return pullResult;
    }

    /**
     * Checks if the git repo exists - If it doesn't runs a git clone. If it does - runs a git pull
     * @throws GitAPIException
     * @throws IOException
     */
    public static void checkAndUpdateGit() throws GitAPIException, IOException {
        logger.info("Checks for git repository and runs a pull");
        Config config = Setup.getConfig();
        Path localPath = Paths.get(config.getLocal_directory()).toAbsolutePath();
        if (! Files.exists(localPath) ) {
            logger.info("Repo does not exist - cloning " + config.getRepo().getUrl() + " to " + config.getLocal_directory());
            GitApp.cloneRepo(config.getRepo().getUrl(), config.getLocal_directory(), config.getRepo().getUser(), config.getRepo().getPassword());
        } else {
            logger.debug("Running git pull");
            GitApp.pull(localPath.toString());
        }
    }
}