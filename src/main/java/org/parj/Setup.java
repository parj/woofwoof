package org.parj;

import org.parj.yaml.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Setup {
    public static Config config ;
    private static String configFile = "src/test/resources/yaml/config.yml";

    private static Logger logger = LoggerFactory.getLogger(Setup.class);

    public static void readConfigFile() throws FileNotFoundException {
        if ( config == null ) {
            logger.info("Reading in config file");
            Yaml yaml = new Yaml();
            InputStream in = new FileInputStream(new File(configFile));
            config = yaml.loadAs(in, Config.class);
        }
    }

    public static void readConfigFile(String configFile) throws FileNotFoundException {
        setConfigFile(configFile);
        readConfigFile();
    }

    public static Config getConfig() throws FileNotFoundException {
        if ( config == null ) {
            readConfigFile();
        }
        return config;
    }

    public static void setConfigFile(String configFile_) {
        configFile = configFile_;
    }
}
