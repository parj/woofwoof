package org.parj.web;

import org.parj.yaml.change.Change;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Controller
public class GetTestConfigController {

    @GetMapping("/gettestconfig")
    @ResponseBody
    public Change returnConfig(@RequestParam(name="name", required=false, defaultValue="src/test/resources/yaml/Rel_AN89812.yml") String name) {
        Yaml yaml = new Yaml();
        try {
            InputStream in = new FileInputStream(new File("src/test/resources/yaml/Rel_AN89812.yml"));
            Change change = yaml.loadAs(in, Change.class);
            return change;
        } catch(FileNotFoundException e) {
            return null;
        }
    }
}
