package org.parj.web;

import org.parj.DeployAChange;
import org.parj.yaml.DeploymentResponse;
import org.parj.yaml.change.Change;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class ExecuteChangeController {
    private DeployAChange deployAChange;

    @GetMapping("/execute")
    @ResponseBody
    public DeploymentResponse executeChange(@RequestParam(name = "change", required = true) String change) {
        return DeployAChange.start(change);
    }
}
