package org.parj.web;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.parj.git.GitApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Webhook for a Git server to send a trigger to run an update
 */
@RestController
public class UpdateGitController {

    private static Logger logger = LoggerFactory.getLogger(UpdateGitController.class);

    @PostMapping("/updategit")
    public void updateGit(@RequestBody Object object) {
        try {
            GitApp.checkAndUpdateGit();
        } catch (GitAPIException e) {
            logger.info("Unable to update git");
            e.printStackTrace(System.err);
        } catch (IOException e) {
            logger.info("Unable to update git");
            e.printStackTrace();
        }
    }


}
