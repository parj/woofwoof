package org.parj.yaml.change;

import org.parj.yaml.change.Deployment;
import org.parj.yaml.change.Rollback;

import java.util.List;

public class Change {
    private String change_number;
    private String description;
    private List<String> impacted_servers;
    private List<String> approvers;
    private Deployment deployment;
    private Rollback rollback;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getApprovers() {
        return approvers;
    }

    public void setApprovers(List<String> approvers) {
        this.approvers = approvers;
    }

    public List<String> getImpacted_servers() {
        return impacted_servers;
    }

    public void setImpacted_servers(List<String> impacted_servers) {
        this.impacted_servers = impacted_servers;
    }

    public Deployment getDeployment() {
        return deployment;
    }

    public void setDeployment(Deployment deployment) {
        this.deployment = deployment;
    }

    public Rollback getRollback() {
        return rollback;
    }

    public void setRollback(Rollback rollback) {
        this.rollback = rollback;
    }

    public String getChange_number() {
        return change_number;
    }

    public void setChange_number(String change_number) {
        this.change_number = change_number;
    }
}
