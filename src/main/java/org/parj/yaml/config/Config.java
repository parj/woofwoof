package org.parj.yaml.config;

import org.parj.yaml.config.Repo;

public class Config {
    private Repo repo;
    private String local_directory;

    public Repo getRepo() {
        return repo;
    }

    public void setRepo(Repo repo) {
        this.repo = repo;
    }

    public String getLocal_directory() {
        return local_directory;
    }

    public void setLocal_directory(String local_directory) {
        this.local_directory = local_directory;
    }
}