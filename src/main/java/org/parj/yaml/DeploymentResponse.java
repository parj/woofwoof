package org.parj.yaml;

public class DeploymentResponse {
    private int exitCode;
    private String sysout;

    public DeploymentResponse(int exitCode, String sysout) {
        setExitCode(exitCode);
        setSysout(sysout);
    }

    public int getExitCode() {
        return exitCode;
    }

    public void setExitCode(int exitCode) {
        this.exitCode = exitCode;
    }

    public String getSysout() {
        return sysout;
    }

    public void setSysout(String sysout) {
        this.sysout = sysout;
    }
}
