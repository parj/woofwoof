package org.parj;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.parj.git.GitApp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileNotFoundException;
import java.io.IOException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws FileNotFoundException, GitAPIException, IOException {
        Setup.readConfigFile();
        GitApp.checkAndUpdateGit();
        SpringApplication.run(Application.class, args);
    }

}