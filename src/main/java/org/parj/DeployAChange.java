package org.parj;

import com.baeldung.RunProcess;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.parj.git.GitApp;
import org.parj.yaml.DeploymentResponse;
import org.parj.yaml.change.Change;
import org.parj.yaml.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DeployAChange {
    private static Logger logger = LoggerFactory.getLogger(DeployAChange.class);
    private static boolean checkedGitRepo = false;

    public static DeploymentResponse start(String changeNumber) {
        try {

            if (!checkedGitRepo) {
                GitApp.checkAndUpdateGit();
                checkedGitRepo = true;
            }
            Change change = slurpYaml(changeNumber);

            return RunProcess.run("uname -a");

        } catch (FileNotFoundException e) {
            logger.info("Unable to read change file");
            e.printStackTrace(System.err);
        } catch (GitAPIException e) {
            logger.info("Unable to update Git");
            e.printStackTrace(System.err);
        } catch(IOException e) {
            logger.info("Unable to do a deployment");
            e.printStackTrace(System.err);
        } catch(InterruptedException e) {
            logger.info("Unable to run command");
            e.printStackTrace(System.err);
        }

        return new DeploymentResponse(1, "Unknown error");
    }

    private static Change slurpYaml(String change) throws FileNotFoundException {
        logger.info("Reading in yaml file");
        Config config = Setup.getConfig();
        Path pathToFile = Paths.get(config.getLocal_directory() + "/" + change + ".yml").toAbsolutePath();
        logger.debug("Reading in file - " + pathToFile.toString());
        Yaml yaml = new Yaml();
        InputStream in = new FileInputStream(new File(pathToFile.toString()));
        return yaml.loadAs(in, Change.class);
    }
}
